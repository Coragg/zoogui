from GUI.menu import MenuPrincipal
from tkinter import *


class Login:
    """ LOGIN
    Only verificate if the user is valid or not. 
    If the user is valid, it call the menu
    if the user is invalid, it prompts to enter a valid user. 
    """
    def __init__(self) -> None:
        self.title = "Login"
        self.size = "500x250"

    def __callMenu(self) -> None:
        self.window.withdraw() 
        menu = MenuPrincipal()
        menu.load()
        menu.show()

    def __confirmUser(self) -> None:
        self.not_valid.pack_forget()
        if self.inputUserName.get() == "admin" and self.passwordInput.get() == "admin":
            self.__callMenu()
        else:
            self.not_valid = Label(self.window, text="User not valid, please enter a valid user")
            self.not_valid.pack()

    def __usePack(self) -> None:
        self.userLabel.pack()
        self.inputUserName.pack()
        self.passwordLabel.pack()
        self.passwordInput.pack()
        self.confirm_data.pack()

    def __interface(self) -> None:
        self.userLabel = Label(self.window, text="User name:")
        self.inputUserName = Entry(self.window, width=50)
        self.passwordLabel = Label(self.window, text="Password:")
        self.passwordInput = Entry(self.window, show="*",width=50)
        self.confirm_data = Button(self.window, text="Send", command=self.__confirmUser)
        self.not_valid = Label(self.window, text="")
        self.__usePack()

    def load(self) -> None:
        self.window = Tk()
        self.window.geometry(self.size)
        self.window.title(self.title)
        self.__interface()

    def show(self) -> Tk:
        self.window.mainloop()
