from tkinter import *
import os


class MenuPrincipal:
    """ Menu
    Buttons to view another GUI are only displayed if the user clicks on them  
    """

    def __init__(self) -> None:
        self.title = "Menu"
        self.size = "500x500"

    def __addAnimal(self) -> None:
        pass

    def __tableAniamal(self) -> None:
        pass

    def __exitSystem(self) -> None:
        os.system("cls")
        self.window.destroy()


    def __buttonMenu(self) -> None:
        buttonCreateAnimal = Button(self.window, text="Add animal", command=self.__addAnimal)
        buttonTableAnimal = Button(self.window, text="Table animal", command=self.__tableAniamal)
        buttonExit = Button(self.window, text="Exit", command=self.__exitSystem)
        buttonCreateAnimal.pack()
        buttonTableAnimal.pack()
        buttonExit.pack()


    def load(self) -> None:
        self.window = Tk()
        self.window.geometry(self.size)
        self.window.title(self.title)
        self.__buttonMenu()

    def show(self) -> Tk:
        self.window.mainloop()


