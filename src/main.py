from GUI.login import Login


def main() -> None:
    start_program = Login()
    start_program.load()
    start_program.show()


if __name__ == '__main__':
    main()
